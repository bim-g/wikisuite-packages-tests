#!/bin/bash

#
# => Installer configuration and defaults
#

FEATURES_LIST=('ssl' 'ftp' 'dns' 'mail')
FEATURES_DEFAULT=('ssl')
FEATURES_POSSIBLE_VALUES=('all' 'none' ${FEATURES_LIST[@]})

BUNDLE_LIST=('LAMP' 'LEMP')
BUNDLE_DEFAULT='LAMP'
BUNDLE_POSSIBLE_VALUES=(${BUNDLE_LIST[@]})

PACKAGES_LIST=('elasticsearch' 'manticore' 'virtualmin-syncthing' 'sysadmin' 'virtualmin-tikimanager' 'content-processing')
PACKAGES_DEFAULT=('virtualmin-tikimanager' 'virtualmin-syncthing' 'sysadmin' 'content-processing') # same as wikisuite-virtualmin-all
PACKAGES_BASE_LIST=('wikisuite-virtualmin-base') # from wikisuite-virtualmin-all, non optional
PACKAGES_POSSIBLE_VALUES=('all' ${PACKAGES_LIST[@]})

PHP_VERSIONS_LIST=('5.6' '7.1' '7.2' '7.3' '7.4' '8.0' '8.1' '8.2' '8.3')
PHP_VERSIONS_DEFAULT=('7.4' '8.1' '8.2' '8.3') # same as wikisuite-virtualmin-all
PHP_VERSIONS_POSSIBLE_VALUES=('all' ${PHP_VERSIONS_LIST[@]})

#
# => Supporting Function declaration
#

usage () {
  echo
  echo "Usage: $1 [-h] [-b <LAMP|LEMP>] [-f <FEATURES>] [-o <PACKAGES>] [-p <VERSIONS>] [-H <HOST>] [-I] [-r <FOLDER>]"
  echo
  echo "Options:"
  echo "  -h This Help"
  echo "  -b <BUNDLE> Bundle to use, possible values: LAMP (Apache) or LEMP (Nginx). Default: ${BUNDLE_DEFAULT}"
  echo "  -f <FEATURES> comma separared list of features to install (${FEATURES_LIST[*]}) or 'all', 'none'. Default: ${FEATURES_DEFAULT[*]}"
  echo "  -o <PACKAGES> comma separared list of packages to install (${PACKAGES_LIST[*]}) or 'all'. Default: ${PACKAGES_DEFAULT[*]}"
  echo "  -p <VERSIONS> comma separared list of PHP versions to install (${PHP_VERSIONS_LIST[*]}) or 'all'. Default: ${PHP_VERSIONS_DEFAULT[*]}"
  echo "  -H <HOST> Hostname to use by the installer script, e.g. server.example.com, Default: $(hostname)"
  echo "  -I Skips the hostname validations (being a fqdn and resolvable)."
  echo "  -r <FOLDER> Use a folder as the repository in APT for wikisuite packages instead of the website, useful for testing. "
  echo
}

feature_enable_generic() {
  local label=$1
  local feature=$2
  local service=$3
  run_fail_ok "virtualmin set-global-feature --enable-feature ${feature}" "${label}: Enabling Virtualmin ${feature} feature"
  run_fail_ok "virtualmin set-global-feature --default-on ${feature}" "${label}: Set default for Virtualmin ${feature} feature to on"
  if [ -n "${service}" ]; then
    run_fail_ok "systemctl enable ${service}" "${label}: Enabling service ${service}"
    run_fail_ok "systemctl restart ${service}" "${label}: Starting service ${service}"
  fi
}

feature_disable_generic() {
  local label=$1
  local feature=$2
  local service=$3
  run_fail_ok "virtualmin set-global-feature --default-off ${feature}" "${label}: Set default for Virtualmin ${feature} feature to off"
  run_fail_ok "virtualmin set-global-feature --disable-feature ${feature}" "${label}: Disabling Virtualmin ${feature} feature"
  if [ -n "${service}" ]; then
    run_fail_ok "systemctl stop ${service}" "${label}: Stopping service ${service}"
    run_fail_ok "systemctl disable ${service}" "${label}: Disabling service ${service}"
  fi
}

feature_enable_ssl() {
  if [ "${BUNDLE}" == 'LEMP' ] ; then
    feature_enable_generic "SSL" "virtualmin-nginx-ssl"
  else
    feature_enable_generic "SSL" "ssl"
  fi
}

feature_disable_ssl() {
  if [ "${BUNDLE}" == 'LEMP' ] ; then
    feature_disable_generic "SSL" "virtualmin-nginx-ssl"
  else
    feature_disable_generic "SSL" "ssl"
  fi
}

feature_enable_ftp() {
  feature_enable_generic "FTP" "ftp" "proftpd.service"
}

feature_disable_ftp() {
  feature_disable_generic "FTP" "ftp" "proftpd.service"
}

feature_enable_dns() {
  feature_enable_generic "DNS" "dns" "named.service"
}

feature_disable_dns() {
  feature_disable_generic "DNS" "dns" "named.service"
}

feature_enable_mail() {
  feature_enable_generic "EMAIL" "mail" "dovecot.service"

  # TODO: if we are setting email, should we handle spam and virus?
  # feature_enable_generic "EMAIL" "spam"
  # feature_enable_generic "EMAIL" "virus"

  run "sed -i 's/^inet_interfaces *= *.*$/inet_interfaces = all/' /etc/postfix/main.cf" "EMAIL: Update postfix to accept remote connections"
  run "systemctl restart postfix.service" "EMAIL: Restarting postfix service"
}

feature_disable_mail() {
  run "sed -i 's/^inet_interfaces *= *all/inet_interfaces = loopback-only/' /etc/postfix/main.cf" "EMAIL: Update postfix not to accept remote connections"
  run "systemctl restart postfix.service" "EMAIL: Restarting postfix service"

  feature_disable_generic "EMAIL" "mail" "dovecot.service"

  # TODO: if we are setting email, should we handle spam and virus?
  # feature_disable_generic "EMAIL" "virus"
  # feature_disable_generic "EMAIL" "spam"
}

check_value_in_array() {
  local VALUE=$1
  shift
  local ARRAY=("$@")
  local elem
  for elem in "${ARRAY[@]}" ; do
    if [ "$elem" == "$VALUE" ] ; then
      return 0
    fi
  done
  return 1
}

check_substr_in_array() {
  local VALUE=$1
  shift
  local ARRAY=("$@")
  local elem
  for elem in "${ARRAY[@]}" ; do
    if [[ "$elem" == *"$VALUE"* ]] ; then
      return 0
    fi
  done
  return 1
}

check_array_contains_array() {
  local tmp
  tmp="${1}[@]"
  local haystack=( "${!tmp}" )
  tmp="${2}[@]"
  local needles=( "${!tmp}" )
  local n
  for n in ${needles[@]} ; do
    if ! check_value_in_array $n ${haystack[@]} ; then
      return 1
    fi
  done
  return 0
}

# similar to slib setconfig, that is not working
configvalue() {
  sc_config="$2"
  sc_value="$1"
  sc_directive=$(echo "$sc_value" | cut -d'=' -f1 | sed 's/^[ \t]*//;s/[ \t]*$//')
  if grep -q "$sc_directive" "$2"; then
    sed -i -e "s#$sc_directive.*#$sc_value#" "$sc_config"
  else
    echo "$1" >>"$2"
  fi
}

# Helper functions (extensions to slib - that will be imported later)
run() {
  RUN_ERRORS_FATAL=1
  log_debug "Running command: $1"
  run_ok "$1" "$2"
  RUN_ERRORS_FATAL=
}

run_fail_ok() {
  log_debug "Running: $1"
  run_ok "$1" "$2"
}

run_file_exists() {
  if [ ! -f "$3" ] || [ ! -d "$3" ] ; then
    return 0
  fi
  run "$1" "$2"
}

run_file_missing() {
  if [ -f "$3" ] || [ -d "$3" ] ; then
    return 0
  fi
  run "$1" "$2"
}

run_fail_ok_file_missing() {
  if [ -f "$3" ]; then
    return 0
  fi
  run_fail_ok "$1" "$2"
}

fix_dns_disable_updates() {
  if [ -x "/usr/sbin/resolvconf" ];
  then
    /usr/sbin/resolvconf --disable-updates
  elif [ -f "/etc/NetworkManager/NetworkManager.conf" ];
  then
    sed -i -e 's/^\[main\]/[main]\ndns=none/' /etc/NetworkManager/NetworkManager.conf
    systemctl restart NetworkManager
  fi
}

fix_default_network_interface_not_auto() {
  if [ -f /etc/network/interfaces ] ; then
    local DEFAULTIF=$(ip route | grep default | sed 's/.*dev \([^ ]*\)[ ]*/\1/')
    ip addr show $DEFAULTIF > /dev/null 2>&1 && (
        cat /etc/network/interfaces /etc/network/interfaces.d/* 2>/dev/null \
        | grep -qE "^[[:space:]]*auto[[:space:]]+${DEFAULTIF}" \
        || (
          echo "auto ${DEFAULTIF}" >> /etc/network/interfaces
          echo
          echo "Fixing default network interface ${DEFAULTIF} not being set as auto ..."
          echo
        )
    )
  fi
}





#
# => main script execution
#

readlink /proc/$$/exe | grep -q bash
RET=$?
if [ $RET != 0 ]; then
  echo
  echo "This script is intended to run using bash, you can either:"
  echo "chmod u+x $0 && ./$0"
  echo "or /bin/bash $0"
  echo
  exit 1
fi

if [ $(id -u) != "0" ]; then
  echo
  echo "This script is intended to be run as root, you may want to try using sudo:"
  echo "sudo $0 $*"
  echo
  exit 1
fi

# Make sure cwd is the same folder as the script.
cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1

# Process command line arguments
BUNDLE=""
HOST=""
IGNORE_HOST="n"
WIKISUITE_DEV_INSTALL_FROM_FOLDER="https://wikisuite-repos.gitlab.io/wikisuite-packages-tests"
FEATURES=()
PACKAGES=()
PHP_VERSIONS=()

while getopts ":hb:f:o:p:H:Ir:" options ; do
  case "${options}" in
    h)
      usage $0
      exit 0
      ;;
    b)
      BUNDLE=${OPTARG}
      if ! check_value_in_array "$BUNDLE" ${BUNDLE_POSSIBLE_VALUES[@]} ; then
        usage $0
        exit 1
      fi
      ;;
    f)
      IFS=', ' read -r -a LIST <<< "${OPTARG}"
      for i in "${LIST[@]}" ; do
        if ! check_value_in_array $i ${FEATURES_POSSIBLE_VALUES[@]} ; then
          echo "Value ($i) not allowed, possible values ${FEATURES_POSSIBLE_VALUES[*]}"
          usage $0
          exit 1
        fi
        if [ "$i" == "all" ] ; then
          FEATURES+=(${FEATURES_LIST[@]})
        else
          FEATURES+=("$i")
        fi
      done
      ;;
    o)
      IFS=', ' read -r -a LIST <<< "${OPTARG}"
      for i in "${LIST[@]}" ; do
        if ! check_value_in_array $i ${PACKAGES_POSSIBLE_VALUES[@]} ; then
          echo "Value ($i) not allowed, possible values ${PACKAGES_POSSIBLE_VALUES[*]}"
          usage $0
          exit 1
        fi
        if [ "$i" == "all" ] ; then
          PACKAGES+=(${PACKAGES_LIST[@]})
        else
          PACKAGES+=("$i")
        fi
      done
      ;;
    p)
      IFS=', ' read -r -a LIST <<< "${OPTARG}"
      for i in "${LIST[@]}" ; do
        if ! check_value_in_array $i ${PHP_VERSIONS_POSSIBLE_VALUES[@]} ; then
          echo "Value ($i) not allowed, possible values ${PHP_VERSIONS_POSSIBLE_VALUES[*]}"
          usage $0
          exit 1
        fi
        if [ "$i" == "all" ] ; then
          PHP_VERSIONS+=(${PHP_VERSIONS_LIST[@]})
        else
          PHP_VERSIONS+=("$i")
        fi
      done
      ;;
    H)
      HOST=${OPTARG}
      ;;
    I)
      IGNORE_HOST='y'
      ;;
    r)
      WIKISUITE_DEV_INSTALL_FROM_FOLDER=${OPTARG}
      if [ ! -d "${WIKISUITE_DEV_INSTALL_FROM_FOLDER}" ] ; then
        echo "The packages root folder does not exist"
        usage $0
        exit 1
      fi
      if [ ! -f "${WIKISUITE_DEV_INSTALL_FROM_FOLDER}/GPG_PUBLIC_KEY" ] ; then
        echo "The packages folder provided does not have the right content/format"
        usage $0
        exit 1
      fi
      ;;
    *)
      usage $0
      exit 1
      ;;
  esac
done

# Apply defaults if no command line arguments were provided
if [ -z "$BUNDLE" ] ; then
  BUNDLE=$BUNDLE_DEFAULT
fi
if [ ${#FEATURES[@]} -eq 0 ] ; then
  FEATURES=(${FEATURES_DEFAULT[@]})
fi
if [ ${#PACKAGES[@]} -eq 0 ] ; then
  PACKAGES=(${PACKAGES_DEFAULT[@]})
fi
if [ ${#PHP_VERSIONS[@]} -eq 0 ] ; then
  PHP_VERSIONS=(${PHP_VERSIONS_DEFAULT[@]})
fi

# Validate Host input
if [ -z "$HOST" ]; then
  HOST=$(hostname)
fi

if [ "$HOST" == "${HOST/./}" ]; then
  if [ "_${IGNORE_HOST}" != "_y" ]; then
    echo
    echo "It is expected for the server to have a fully qualified domain name (FQDN)."
    echo "More info is available at the Fully qualified domain name section of:"
    echo "https://www.virtualmin.com/documentation/installation/automated"
    echo
    echo "You can use the following command. Please remember to"
    echo "replace server1.example.org by your real (sub)domain name"
    echo
    echo "sudo hostnamectl set-hostname server1.example.org"
    echo
    echo "Then, restart your server with the following command"
    echo "(after which you try again the installation)"
    echo
    echo "reboot"
    echo
    echo "Advanced users may prefer to set the hostname as a parameter of the script"
    echo
    usage $0
    echo
    exit 1
  fi
  echo
  echo "Forcing the script to ignore that the host name (${HOST}) is not a fully qualified domain name (FQDN)."
  echo
fi

HOSTCMD=host
if [ ! command -v host ] &>/dev/null && [ command -v nslookup ] &>/dev/null; then
  HOSTCMD=nslookup
fi

$HOSTCMD $HOST &>/dev/null
RET=$?
if [ $RET != 0 ] ; then
  if [ "_${IGNORE_HOST}" != "_y" ]; then
    echo
    echo "Not able to resolve hostname '$HOST', refusing to continue"
    echo "If you want to force that, set the parameter -I when calling the script"
    echo
    usage $0
    echo
    exit 2
  fi
  echo
  echo "Forcing the script to ignore that the host name (${HOST}) is not resolvable"
  echo
fi

export WIKISUITE_INSTALL_HOST="${HOST}"

# Install essential packages if not installed
missingPackages=();
if [ ! -x "/usr/bin/curl" ]; then
  missingPackages+=('curl')
fi

if [ ! -x "/usr/bin/wget" ]; then
  missingPackages+=('wget')
fi

if [ ! -x "/usr/bin/sudo" ]; then
  missingPackages+=('sudo')
fi

if [ ! -x "/bin/ps" ]; then
  missingPackages+=('procps')
fi

if [ ! -x "/usr/bin/lsb_release" ]; then
  missingPackages+=('lsb-release')
fi

if [ ! -x "/usr/lib/apt/methods/https" ]; then
  missingPackages+=('apt-transport-https')
fi

if (( ${#missingPackages[@]} != 0 )); then
  echo
  echo "Installing ${missingPackages[*]} ..."
  echo
  apt-get update >> /dev/null
  apt-get install -y ${missingPackages[*]}
fi

#
# => Fix potential system issues
#

fix_dns_disable_updates # Deal with some system configurations to avoid network config updates
fix_default_network_interface_not_auto # Fix potential issue with default interface not being set as auto

#
# => Download slib (wikisuite installer dependency)
#

if [ ! -f ./slib.sh ]; then
  echo
  echo "Downloading slib ..."
  echo

  # Download the slib (source: http://github.com/virtualmin/slib)
  # Lots of little utility functions.
  curl -s -S -L https://software.virtualmin.com/lib/slib.sh -o slib.sh
  if [ ! -f ./slib.sh ]; then
    echo
    echo "Library slib.sh could not be downloaded, cancelling the install... "
    echo
    exit 3
  fi
fi

#
# => Install Virtualmin
#

echo
echo "Installing Virtualmin using the Hostname '$HOST' ..."
echo

if [ -f /usr/sbin/virtualmin ]; then
  echo
  echo "* Virtualmin already installed, skipping ..."
  echo
else
  echo
  echo "* Downloading Virtualmin ..."
  echo

  curl -s -S -L https://software.virtualmin.com/gpl/scripts/install.sh -o install.sh

  VIRTUALMIN_INSTALL_TYPE='minimal'
  VIRTUALMIN_INSTALL_TYPE_ARG='--minimal'
  if check_value_in_array 'mail' ${FEATURES[@]} ; then
    # The minimal setup skips mostly components related to email server,
    # If we are setting up a e-mail server then surelly we will want the spam/virus packages and components
    VIRTUALMIN_INSTALL_TYPE='standard'
    VIRTUALMIN_INSTALL_TYPE_ARG=''
  fi

  echo
  echo "* Installing $BUNDLE stack with ${VIRTUALMIN_INSTALL_TYPE} setup ..."
  echo

  /bin/sh install.sh --hostname ${HOST} ${VIRTUALMIN_INSTALL_TYPE_ARG} --bundle ${BUNDLE} --force --yes

  rm install.sh
fi

#
# => Run WikiSuite Installer
#

echo
echo
echo "* Installing WikiSuite..."
echo

# load slib
. ./slib.sh

LOGFILE=wikisuite-installer.log
# slib config
LOG_LEVEL_STDOUT="${LOG_STDOUT:-INFO}"
LOG_LEVEL_LOG="${LOG_LOG:-DEBUG}"
LOG_PATH="$LOGFILE"
RUN_LOG="$LOGFILE"

# slib fixes
if [ "$(declare -Ff "log_fatal")" != "log_fatal" ] ; then
  log_fatal() { log_error "$1"; }
fi

# Discover and sets up distro version globals os_type, os_version, os_major_version, os_real
get_distro

# set variables based on OS type
case "$os_type" in
  "fedora" | "centos" | "rhel" | "amazon")
      ;;
  "debian" | "ubuntu")
      export DEBIAN_FRONTEND=noninteractive
      ;;
esac

log_info "Started WikiSuite installation log in $LOGFILE"

echo

log_debug "Updating Operating System"
printf "${YELLOW}▣${CYAN}□□${NORMAL}: Updating Operating System\\n"

run "apt-get update" "Updating the package repository information"
run "apt-get upgrade -y" "Updating the packages installed"

log_debug "Installing WikiSuite"
printf "${GREEN}▣${YELLOW}▣${CYAN}□${NORMAL}: Installing WikiSuite\\n"

# Configure WikiSuite sources
if [ -n "${WIKISUITE_DEV_INSTALL_FROM_FOLDER}" ] ; then
  # configure wikisuite to use a local folder as source, used to test/validate changes without merge to master
  # Download the artificats from the generatePages job and unzip them in a folder on the server
  printf "${YELLOW}Warning: Installing WikiSuite from a local folder (${WIKISUITE_DEV_INSTALL_FROM_FOLDER}) for tests${NORMAL}\\n"
  run "curl -s -S -L ${WIKISUITE_DEV_INSTALL_FROM_FOLDER}/GPG_PUBLIC_KEY | gpg --no-default-keyring --dearmor > /etc/apt/trusted.gpg.d/wikisuite-packages.gpg" "Download WikiSuite Packages repo pgp key"
  run "echo 'deb ${WIKISUITE_DEV_INSTALL_FROM_FOLDER}/${os_type} $(lsb_release -sc) main' > /etc/apt/sources.list.d/wikisuite.list" "Configure WikiSuite repo source URL"
else
  # Normal - production - usage, we will install from packages.wikisuite.org
  run "curl -s -S -L https://packages.wikisuite.org/GPG_PUBLIC_KEY | gpg --no-default-keyring --dearmor > /etc/apt/trusted.gpg.d/wikisuite-packages.gpg" "Download WikiSuite Packages repo pgp key"
  run "echo 'deb https://packages.wikisuite.org/${os_type} $(lsb_release -sc) main' > /etc/apt/sources.list.d/wikisuite.list" "Configure WikiSuite repo source URL"
fi

run "apt-get update" "Updating apt repositories"

# decide what repositories and packages to install
PKG_INSTALL_LIST=()
INSTALL_WIKISUITE_ALL=1
if check_array_contains_array PACKAGES PACKAGES_DEFAULT && check_array_contains_array PHP_VERSIONS PHP_VERSIONS_DEFAULT ; then
  INSTALL_WIKISUITE_ALL=0
  PKG_INSTALL_LIST+=('wikisuite-virtualmin-all')
fi
if [ ! ${INSTALL_WIKISUITE_ALL} ] ; then
  PKG_INSTALL_LIST+=(${PACKAGES_BASE_LIST[@]})
fi
for p in ${PACKAGES_LIST[@]} ; do
  [ ${INSTALL_WIKISUITE_ALL} -eq 0 ] && check_value_in_array $p ${PACKAGES_DEFAULT[@]} && continue
  if check_value_in_array $p ${PACKAGES[@]} ; then
    PKG_INSTALL_LIST+=( "wikisuite-$p" )
  fi
done
for p in ${PHP_VERSIONS_LIST[@]} ; do
  [ ${INSTALL_WIKISUITE_ALL} -eq 0 ] && check_value_in_array $p ${PHP_VERSIONS_DEFAULT[@]} && continue
  if check_value_in_array $p ${PHP_VERSIONS[@]} ; then
    PKG_INSTALL_LIST+=( "wikisuite-php$p" )
  fi
done

REPO_INSTALL_LIST=()
if [ ${INSTALL_WIKISUITE_ALL} -eq 0 ] ; then
  REPO_INSTALL_LIST+=('wikisuite-external-repositories')
else
  REPO_INSTALL_LIST+=('wikisuite-external-repo-nodejs') # is not linked to optional packages
  check_substr_in_array '-elasticsearch' ${PKG_INSTALL_LIST[@]} && REPO_INSTALL_LIST+=('wikisuite-external-repo-elasticsearch')
  check_substr_in_array '-manticore' ${PKG_INSTALL_LIST[@]} && REPO_INSTALL_LIST+=('wikisuite-external-repo-manticore')
  check_substr_in_array '-php' ${PKG_INSTALL_LIST[@]} && REPO_INSTALL_LIST+=('wikisuite-external-repo-php')
  check_substr_in_array '-syncthing' ${PKG_INSTALL_LIST[@]} && REPO_INSTALL_LIST+=('wikisuite-external-repo-syncthing')
fi

run "apt-get install -y ${REPO_INSTALL_LIST[*]}" "Installing WikiSuite External Repositories"
run "apt-get update" "Updating apt repositories"

run "apt-get install -y ${PKG_INSTALL_LIST[*]}" "Installing WikiSuite Virtualmin"

log_debug "Configuration of Optional Features"
printf "${GREEN}▣▣${YELLOW}▣${NORMAL}: Configuration of Optional Features\\n"

for F in ${FEATURES_LIST[@]} ; do
  if check_value_in_array "$F" ${FEATURES[@]} ; then
    FUNC="feature_enable_$F"
    ACTION="enabling"
    printf "${GREEN}Enabling${NORMAL} feature ${F}...\\n"
  else
    FUNC="feature_disable_$F"
    ACTION="disabling"
    printf "${YELLOW}Disabling${NORMAL} feature ${F}...\\n"
  fi
  [ $(type -t $FUNC) == function ] \
    && $FUNC \
    || printf "${YELLOW}Skipping feature ${F}..., no function defined to handle feature ${ACTION}${NORMAL}\\n"
done

run "webmin set-config -m virtual-server -o default_domain_ssl -v 2" "Setup virtualserver default ssl"

run "virtualmin check-config" "Reloading Virtualmin configuration"
